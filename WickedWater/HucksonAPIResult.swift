//
//  HucksonAPIResult.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-02.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import UIKit
class HucksonAPIResult {
    let status: Int
    let error: String
    let data: String
    let dataList: [String]
    
    init(status: Int, error: String) {
        self.status = status
        self.error = error
        self.data = ""
        self.dataList = []
    }
    
    init(status: Int, error: String, data: String, dataList: [String]) {
        self.status = status
        self.error = error
        self.data = data
        self.dataList = dataList
    }
    

}
