//
//  PointHistoryViewController.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-28.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import UIKit
class PointHistoryViewController: UITableViewController {
    var pointHistory : [String] = []
    override func viewDidLoad() {
        let parameters : [String: String] = ["u": String(AppVariable.userID)]
        
        HucksonAPI.callAPIForResult(Method.pointshistory.rawValue, parameters: parameters,
                                    completion: { apiCallResult -> Void in
                                        if (apiCallResult.status == 1) {
                                            self.pointHistory = apiCallResult.dataList
                                            self.tableView.reloadData()
                                        } else {
                                            //write your own code to show an alert controller with the message from the API
                                            let alert = UIAlertController(title: "Error", message: apiCallResult.error, preferredStyle: UIAlertControllerStyle.alert)
                                            
                                            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                            alert.addAction(okAction)
                                            
                                            self.present(alert, animated:true, completion: nil)

                                        }
        })
    }
    @IBAction func CloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pointHistory.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
        UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "pointHistoryCell", for: indexPath)
            let currentItem = pointHistory[indexPath.row]
            cell.textLabel?.text = currentItem
            return cell
    }


    
}
