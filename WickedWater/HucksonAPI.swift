//
//  HucksonAPI.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-02.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import Foundation
enum Method: String {
    case auth = "/auth/v1/"
    case forgotpassword = "/forgotpassword/v1/"
    case createaccount = "/createaccount/v1/"
    case pointsummary = "/pointsummary/v1/"
    case activeads = "/activeads/v1/"
    case pointshistory = "/pointshistory/v1/"
    case updatepassword = "/updatepassword/v1/"
    case updateprofile = "/updateprofile/v1/"
}
struct HucksonAPI {
    private static let baseURLString = "https://huckson.digitalgrounds.ca/api"
    static func parseResult(fromJSON data: Data) -> HucksonAPIResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            
            guard
                let jsonDictionary = jsonObject as? [AnyHashable:Any],
                let apiStatus = jsonDictionary["status"] as? Int,
                let apiError = jsonDictionary["error"] as? String,
                let apiData = jsonDictionary["data"] as? String,
                let apiDataList = jsonDictionary["dataList"] as? [String] else {
                    return HucksonAPIResult(status: -1, error: "Invalid JSON format")
            }
            
            return HucksonAPIResult(status: apiStatus, error: apiError, data: apiData, dataList:
                apiDataList)
        } catch let error {
            return HucksonAPIResult(status: -1, error: error.localizedDescription)
        }
    }
    public static func callAPIForResult(_ method: String, parameters: [String: String], completion:
        @escaping (HucksonAPIResult) -> Void) {
        let url = URL(string: self.baseURLString + method)!
        var request = URLRequest(url: url)
        
        //this setups up the parameters to be passed using post instead of get
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "ContentType")
        request.httpMethod = "POST"
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data,
                error == nil else {
                    let result = HucksonAPIResult(status: -1, error: "Error contacting server")
                    OperationQueue.main.addOperation {
                        completion(result)
                    }
                    return
            }
            
            let result = HucksonAPI.parseResult(fromJSON: data)
            
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
        
        task.resume()
    }
}
extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters:
                .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters:
                .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
            }
            .joined(separator: "&")
    }
}
extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 -
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
