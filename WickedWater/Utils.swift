//
//  Utils.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-28.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import Foundation
class Utils  {
    
    static func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}
