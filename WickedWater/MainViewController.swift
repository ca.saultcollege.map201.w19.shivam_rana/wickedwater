//
//  MainViewController.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-02.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import UIKit
class MainViewController: UIViewController{
    
    let shapelayer = CAShapeLayer()
    var getString: String = ""
    
    
    
    @IBOutlet weak var PointProgressBar: UIProgressView!
    @IBOutlet weak var AdvertisementImageView: UIImageView!
    @IBOutlet weak var TotalPointsTextField: UITextField!
    @IBOutlet weak var pointView: UIView!
    
    var curAd : Int = 0
    var ads : [String] = []
    private let session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    @objc func nextAd() {
        if ( ads.count > 1) {
            if ( (curAd + 1) == ads.count) {
                curAd = 0
                
            } else {
                curAd = curAd + 1
            }
        }
        self.loadAd()
    }
    @objc func update() {
        PointProgressBar.progress += 0.02
    }
    func loadAd() {
        let photoURL = URL(string: ads[curAd])
        let request = URLRequest(url: photoURL!)
        let task = session.dataTask(with: request) {
            (data, response, error) -> Void in
            guard
                let imageData = data,
                let image = UIImage(data: imageData) else {
                    //show a broken image
                    //do this by finding an image online, importing it into your project
                    //Then set your image view to the image
                    return
            }
            OperationQueue.main.addOperation {
                self.AdvertisementImageView.image = image
            }
        }
        task.resume()
    }
    override func viewDidLoad() {
        super.viewDidLoad()


        let parameters : [String: String] = ["u": String(AppVariable.userID)]
        HucksonAPI.callAPIForResult(Method.pointsummary.rawValue, parameters: parameters,
                                    
                                    completion: { apiCallResult -> Void in
                                        if (apiCallResult.status == 1) {
                                            self.TotalPointsTextField.text = apiCallResult.data
                                            self.getString = apiCallResult.data
                                            
                                        } else {
                                            let alertcontroller = UIAlertController(title: "Error", message:
                                                
                                                apiCallResult.error, preferredStyle: .alert)
                                            
                                            let okAction = UIAlertAction(title:"OK", style: .cancel, handler: nil)
                                            alertcontroller.addAction(okAction)
                                            self.present(alertcontroller, animated: true, completion: nil)
                                        }
                                        
        })
        HucksonAPI.callAPIForResult(Method.activeads.rawValue, parameters: parameters,
                                    
                                    completion: { apiCallResult -> Void in
                                        if (apiCallResult.status == 1) {
                                            self.ads = apiCallResult.dataList
                                            HucksonAPI.callAPIForResult(Method.activeads.rawValue, parameters:
                                                
                                                parameters,
                                                                        
                                                                        completion: { apiCallResult -> Void in
                                                                            if (apiCallResult.status == 1) {
                                                                                self.ads = apiCallResult.dataList
                                                                                self.loadAd();
                                                                                Timer.scheduledTimer(timeInterval: 4.0, target: self,                                                                                             selector: #selector(self.nextAd), userInfo:nil, repeats: true) }
                                                                            
                                            })
                                        } else {
                                           
                                            
                                            let alertcontroller = UIAlertController(title: "Error", message:
                                                
                                                apiCallResult.error, preferredStyle: .alert)
                                            
                                            let okAction = UIAlertAction(title:"OK", style: .cancel, handler: nil)
                                            alertcontroller.addAction(okAction)
                                            
                                            self.present(alertcontroller,animated: true,completion: nil)
                                           
                                        }
                                        
        }
        )
    }
    @IBAction func ShowPointButton(_ sender: Any) {
        print("check getString",getString)
        var showPoint: UILabel {
            
            let lable = UILabel()
            lable.text = getString
            lable.textAlignment = .center
            lable.textColor = UIColor.blue
            lable.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
            lable.center = view.convert(pointView.center, to: pointView)
            return lable
        }
       
        
         pointView.layer.addSublayer(shapelayer)
         pointView.addSubview(showPoint)
        
        let circularPath = UIBezierPath(arcCenter: CGPoint(x:pointView.frame.width/2,y:pointView.frame.height/2), radius: 100, startAngle: CGFloat(Double.pi), endAngle: CGFloat(2*Double.pi+2*Double.pi), clockwise: true)
        
        shapelayer.path = circularPath.cgPath
        shapelayer.strokeColor = UIColor.blue.cgColor
        shapelayer.strokeEnd = 0
        shapelayer.fillColor = UIColor.clear.cgColor
        shapelayer.lineWidth = 10
        
        
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        basicAnimation.toValue = 1
        basicAnimation.duration = 2
       
        
        shapelayer.add(basicAnimation,forKey: "basic" )
        
        
        
    }
    
  
    
    @IBAction func ContactHucksonButton(_ sender: Any) {
        if let url = URL(string: "https://www.huckson.com/contact") {
            UIApplication.shared.open(url, options: [:])
            
        }

    }
    
    @IBAction func ViewPointHistoryButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "pointHistoryViewController")
        self.present(controller, animated: true, completion: nil)
        


        
    }
    
    @IBAction func UpdateProfileButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "profileViewController")
        
        self.present(controller, animated: true, completion: nil)


        
    }
    
    @IBAction func ChangePasswordButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let controller = storyboard.instantiateViewController(withIdentifier: "passwordViewController")
        
        self.present(controller, animated: true, completion: nil)

    }
    
    
}


