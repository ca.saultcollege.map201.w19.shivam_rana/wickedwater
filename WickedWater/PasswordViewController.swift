//
//  PasswordViewController.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-28.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import UIKit
class PasswordViewController: UIViewController {
    
   
    @IBOutlet weak var CurentPasswordTextField: UITextField!
    @IBOutlet weak var NewPasswordTextField: UITextField!
    @IBOutlet weak var ConfirmPasswordTextField: UITextField!
    
    @IBAction func SaveButton(_ sender: Any) {
        //write code to check if the fields are empty and new and confirm passwords match
        //write code to show an alert controller if they are empty or do not match
        // use this code if everything is okay
        if CurentPasswordTextField.text == "" || NewPasswordTextField.text == "" || ConfirmPasswordTextField.text == "" || !(ConfirmPasswordTextField.text == NewPasswordTextField.text)
        {
            let alertController = UIAlertController(title: "Missing Information", message: "Please fill all fields",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(okAction)
            present(alertController, animated: true, completion: nil)
        }
        let parameters : [String: String] = ["u": String(AppVariable.userID), "c": CurentPasswordTextField.text!, "n":NewPasswordTextField.text!, "cp": ConfirmPasswordTextField.text! ]
        
        HucksonAPI.callAPIForResult(Method.updatepassword.rawValue, parameters: parameters,
                                    completion: { apiCallResult -> Void in
                                        if (apiCallResult.status == 1) {
                                            //write code to show an alert controller advising their password has been updated
                                            let alert = UIAlertController(title: "Success", message: "Hey, A password has been Updated", preferredStyle: UIAlertControllerStyle.alert)
                                            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                            alert.addAction(okAction)
                                            self.present(alert, animated:true, completion: nil)

                                            self.dismiss(animated: true, completion: nil)
                                        } else {
                                            //write code to show an alert controller with the message from the API
                                            let alert = UIAlertController(title: "Error", message: apiCallResult.error, preferredStyle: UIAlertControllerStyle.alert)
                                            
                                            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                            alert.addAction(okAction)
                                            
                                            self.present(alert, animated:true, completion: nil)

                                        }
        })
    }
   
    @IBAction func CancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
