//
//  ViewController.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-02-28.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
   
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func LoginButton(_ sender: Any) {
        let getEmailTextField = EmailTextField.text
        let getPasswordTextField = PasswordTextField.text
        if (getEmailTextField == "" || getPasswordTextField == ""){
            let myAlertEmpty = UIAlertController(title: "Alert", message: "Please type something in the Text Field", preferredStyle: UIAlertControllerStyle.alert)
            myAlertEmpty.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(myAlertEmpty,animated: true,completion: nil)
        }
        
        let parameters : [String: String] = ["e" : getEmailTextField!, "p" : getPasswordTextField!]
        
        HucksonAPI.callAPIForResult(Method.auth.rawValue, parameters: parameters,
                                    completion: { apiCallResult -> Void in
                                        if (apiCallResult.status == 1) {
                                            AppVariable.userID = Int(apiCallResult.data)!
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            let controller = storyboard.instantiateViewController(withIdentifier:
                                                "mainViewController")
                                            self.present(controller, animated: true, completion: nil)
                                        } else {
                                            //write your own code to show an alert controller with the message from the API
                                            let alertcontroller = UIAlertController(title: "Error", message:
                                                
                                                apiCallResult.error, preferredStyle: .alert)
                                            
                                            let okAction = UIAlertAction(title:"OK", style: .cancel, handler: nil)
                                            alertcontroller.addAction(okAction)
                                            self.present(alertcontroller, animated: true, completion: nil)
                                        }
                                        print(apiCallResult.error)
        })
        
    }
    
    @IBAction func ForgotPasswordButton(_ sender: Any) {
        let getText = EmailTextField.text
        let myAlertController = UIAlertController(title: "Forgot Password", message: "Please enter the email", preferredStyle: .alert)
        
       
        
        myAlertController.addTextField{ (textField) in
            if(getText == ""){
                textField.placeholder = "Enter your Email"
            }else{
                textField.text = getText!
            }
            
        }

        
              let action = UIAlertAction(title: "OK", style: .default) { (alertAction) in
                let textField = myAlertController.textFields![0] as UITextField
                
                //when the user entered in the popup
                //textField.text
                let e = textField.text
                
                if (Utils.isValidEmail(testStr: e!) == false) {
                    let alert = UIAlertController(title: "Error", message: "Email address is not valid", preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alert.addAction(okAction)
                    self.present(alert, animated:true, completion: nil)
                } else {
        
       
            let parameters : [String: String] = ["e": textField.text!]
        
        HucksonAPI.callAPIForResult(Method.forgotpassword.rawValue, parameters:
            parameters, completion: { apiCallResult -> Void in
                if (apiCallResult.status == 1) {
                  
                    let alertcontroller = UIAlertController(title: "Email sent", message: "A password reset email has been sent to your email address.", preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertcontroller.addAction(okAction)
                    self.present(alertcontroller, animated: true, completion: nil)
                } else {
                   
                    let alertcontroller = UIAlertController(title: "Error", message: apiCallResult.error,
                                                            
                                                            preferredStyle: .alert)
                    
                    let okAction = UIAlertAction(title:"OK", style: .cancel, handler: nil)
                    alertcontroller.addAction(okAction)
                    self.present(alertcontroller, animated: true, completion: nil)
                }
        })
                }
                
            }
        myAlertController.addAction(action)
        self.present(myAlertController,animated: true,completion: nil)
    }
    @IBAction func CreatePasswordButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier:
            "createAccountViewController")
        self.present(controller, animated: true, completion: nil)
    }
    
   
}

