//
//  CreateAccountViewController.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-02.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import UIKit
class CreateAccountViewController: UIViewController {
    
    @IBOutlet weak var FirstNameTextField: UITextField!
    @IBOutlet weak var LastNameTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    @IBOutlet weak var ConfirmPasswordTextField: UITextField!
    @IBOutlet weak var PhoneTextField: UITextField!
    
    @IBAction func SignUpButton(_ sender: Any) {
        
        let getFirstName = FirstNameTextField.text
        let getLastName = LastNameTextField.text
        let getEmail = EmailTextField.text
        let getPassword = PasswordTextField.text
        let getRepeatPassword = ConfirmPasswordTextField.text
        let getPhone = PhoneTextField.text
        
        if (getFirstName == "" || getLastName == "" || getEmail == "" || getPassword == "" || getRepeatPassword == "" || getPhone == "") {
            let myAlertEmpty = UIAlertController(title: "Alert", message: "Please type something", preferredStyle: UIAlertControllerStyle.alert)
            myAlertEmpty.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(myAlertEmpty,animated: true,completion: nil)
        }
        
        if (getPassword != getRepeatPassword){
            let myAlert = UIAlertController(title: "Alert", message: "Password not match", preferredStyle: UIAlertControllerStyle.alert)
            myAlert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))
            self.present(myAlert,animated: true,completion:nil)
        }
       
        let parameters : [String: String] = ["e": getFirstName!, "f": getLastName!, "l": getEmail!, "p": getPassword!, "cp": getRepeatPassword!, "ph": getPhone!]
        
        HucksonAPI.callAPIForResult(Method.createaccount.rawValue, parameters: parameters,
                                    completion: { apiCallResult -> Void in
                                        if (apiCallResult.status == 1) {
                                            AppVariable.userID = Int(apiCallResult.data)!
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            let controller = storyboard.instantiateViewController(withIdentifier:
                                                "mainViewController")
                                            self.present(controller, animated: true, completion: nil)
                                        } else {
                                            let alertcontroller = UIAlertController(title: "Error", message:
                                                
                                                apiCallResult.error, preferredStyle: .alert)
                                            
                                            let okAction = UIAlertAction(title:"OK", style: .cancel, handler: nil)
                                            alertcontroller.addAction(okAction)
                                            self.present(alertcontroller, animated: true, completion: nil)
                                        }
        })
    }
    @IBAction func CancelButton(_ sender: Any) {
        self.presentingViewController?.dismiss(animated: false, completion:nil)
    }
}
