//
//  ProfileViewController.swift
//  WickedWater
//
//  Created by Shivam Rana on 2019-03-28.
//  Copyright © 2019 Big Nerd Ranch. All rights reserved.
//

import UIKit
class ProfileViewController: UIViewController {
    
    @IBOutlet weak var FirstnameTextField: UITextField!
    
    @IBOutlet weak var LastnameTextField: UITextField!
    @IBOutlet weak var EmailTextField: UITextField!
    @IBOutlet weak var PhoneTextField: UITextField!

    @IBAction func SaveButton(_ sender: Any) {
        //write your own code to check if the mandatory fields are empty
        //write your own code to show an alert controller if they are empty
        // use this code if everything is okay
        if FirstnameTextField.text == "" || LastnameTextField.text == "" || EmailTextField.text == "" || PhoneTextField.text == "" {
            let alertController = UIAlertController(title: "Missing Information", message: "Please fill up all fields", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(okAction)
            
            present(alertController, animated: true, completion: nil)
        }
        let parameters : [String: String] = [ "u": String(AppVariable.userID), "f": FirstnameTextField.text!, "l": LastnameTextField.text!, "ph": PhoneTextField.text!, "e": EmailTextField.text!]
        
        HucksonAPI.callAPIForResult(Method.updateprofile.rawValue, parameters: parameters,
                                    completion: { apiCallResult -> Void in
                                        if (apiCallResult.status == 1) {
                                            //write code to show an alert controller advising their profile has been updated
                                            let alert = UIAlertController(title: "Success", message: "Hey, Your Profile has been Updated", preferredStyle: UIAlertControllerStyle.alert)
                                            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                            alert.addAction(okAction)
                                            self.present(alert, animated:true, completion: nil)

                                            //self.dismiss(animated: true, completion: nil)
                                        } else {
                                            //write your own code to show an alert controller with the message from the API
                                            let alert = UIAlertController(title: "Error", message: apiCallResult.error, preferredStyle: UIAlertControllerStyle.alert)
                                            
                                            let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                            alert.addAction(okAction)
                                            
                                            self.present(alert, animated:true, completion: nil)

                                        }
        })
    }
    
    @IBAction func CancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
